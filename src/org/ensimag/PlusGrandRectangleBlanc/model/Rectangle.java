package org.ensimag.PlusGrandRectangleBlanc.model;

/**
 * Class that represent a rectangle.
 * Its instances can be : rectangle patterns (only including height and width) or specified rectangle (including height,width,position)
 * @author vince
 *
 */

public class Rectangle {
	
	public int posY;
	public int posX;
	public int height;
	public int width;

	public static final Rectangle nullRectangle = new Rectangle(0, 0, 0, 0);

	
	public Rectangle(Rectangle r) {
		super();
		this.height = r.getHeight();
		this.width = r.getWidth();
		this.posX = r.getPosX();
		this.posY = r.getPosY();
	}
	
	public Rectangle(int posY, int posX, int height, int width) {
		super();
		this.height = height;
		this.width = width;
		this.posX = posX;
		this.posY = posY;
	}
	

	public Rectangle(int height, int width) {
		this.height = height;
		this.width = width;
		this.posX = 0;
		this.posY = 0;
	}
	
	public int getNbCases(){
		return this.width*this.height;
	}
	
	public String toString(){
		return "Rectangle :  posY = "+this.posY+" posX = "+this.posX+" ; height = "+this.height+" ; width = "+this.width;
	}

	//ACCESSORS
	
	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + height;
		result = prime * result + posX;
		result = prime * result + posY;
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rectangle other = (Rectangle) obj;
		if (height != other.height)
			return false;
		if (posX != other.posX)
			return false;
		if (posY != other.posY)
			return false;
		if (width != other.width)
			return false;
		return true;
	}
	
	
}
