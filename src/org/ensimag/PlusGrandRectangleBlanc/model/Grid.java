package org.ensimag.PlusGrandRectangleBlanc.model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

import org.ensimag.PlusGrandRectangleBlanc.algorithms.Solution4;
import org.ensimag.PlusGrandRectangleBlanc.algorithms.Solutions;
import org.ensimag.PlusGrandRectangleBlanc.algorithms.Solution1;
import org.ensimag.PlusGrandRectangleBlanc.algorithms.Solution2;
import org.ensimag.PlusGrandRectangleBlanc.algorithms.Solution3;
import org.ensimag.PlusGrandRectangleBlanc.tests.Counter;
import org.ensimag.PlusGrandRectangleBlanc.tools.InputFileFormatException;

public class Grid {
	private boolean[][] array;
	private int[][] heights;

	public Grid(boolean[][] grid) {
		this.array = grid;
	}

	public Rectangle findBiggestWhiteRectangle(Solutions algorithm){
		if(this.getArray() != null){
			switch(algorithm){
			case Solution1: return Solution1.compute(this);
			case Solution2: return Solution2.compute(this);
			case Solution3: return Solution3.compute(this);
			case Solution4: return Solution4.compute(this);
			default: return null;
			}
		}
		return null;
	}

	public boolean isRectangleValid(Rectangle rectangle){
		for(int i = rectangle.getPosY();i<rectangle.getPosY()+rectangle.getHeight();i++){
			for(int j = rectangle.getPosX();j<rectangle.getPosX()+rectangle.getWidth();j++){
				Counter.nbLoop56++;
				if(!this.getArray()[i][j])
					return false;
			}
		}
		return true;
	}

	//Create a boolean array representing the rectangle grid from a given file
	public static Grid getGrid(String filename) throws InputFileFormatException{

		BufferedReader br = null;
		String line = null;

		//Rectangle Array
		int actualLine = 0;
		int lineNumber = 0;
		int colummnNumber = 0;
		boolean[][] booleanArray = null;

		try {
			br = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		if(br != null){
			try {
				line = br.readLine();

				try{

					lineNumber = Integer.parseInt(line.substring(0, line.indexOf(' ')));
					colummnNumber = Integer.parseInt(line.substring(line.indexOf(' ')+1).replaceAll(" ", ""));


					booleanArray = new boolean[lineNumber][colummnNumber];
				}
				catch(NumberFormatException | StringIndexOutOfBoundsException e){
					throw new InputFileFormatException("\nInvalid File Format :\nYou must set the first line as the following pattern :\n\"\'lineNumber\' \'columnNumber\'\"");
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

			while (line != null) {
				try {
					line = br.readLine();

					if(line!=null){
						try{
							for(int i=0;i<colummnNumber;i++){
								switch(Integer.parseInt(line.substring(i, i+1))){
								case 0 : booleanArray[actualLine][i] = true;
								break;
								case 1 : booleanArray[actualLine][i] = false;
								break;	
								}
							}
						}
						catch(NumberFormatException | StringIndexOutOfBoundsException e){
							throw new InputFileFormatException("\nInvalid File Format :\nYou must set the grid lines as the following pattern :\n00100 (for example)\nThe line and column number must fit with the grid definition");
						}
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
				actualLine++;
			}

			return new Grid(booleanArray);
		}
		else
			return null;
	}

	public void createHeightsGrid(){
		this.setHeights(new int[this.getArray().length][this.getArray()[0].length]);

		for(int i = 0;i<this.getArray().length;i++){
			for(int j = 0;j<this.getArray()[0].length;j++){

				//If the current case is white
				if(this.getArray()[i][j]){

					if(i!=0){
						//We can increment the previous value
						this.getHeights()[i][j] = this.getHeights()[i-1][j]+1;
					}
					//If we are on the first line, the previous line doesn't exist. This fictive line is filled by zeros.
					else
						this.getHeights()[i][j] = 1;// So we set it to 1 (0+1)
				}
				//If the current case is black
				else{
					this.getHeights()[i][j] = 0;//If the current case is black
				}
			}
		}
	}
/*
	public static Grid createRandomGrid(int height, int width, float pWhite){
		Grid grid = new Grid(new boolean[height][width]);
		int nbWhite = 0;
		int nbMax = (int) (height * width * pWhite);
		Random rand = new Random();
		int x;
		
		Method 1 to create a random grid
		
//		for(int i = 0; i < height; i++){
//			for(int j = 0; j < width; j++){
//				if(nbWhite < nbMax){
//					x = rand.nextInt(2);
//					nbWhite += (x == 0) ? 1 : 0;
//				}else
//					x = 1;
//				grid.array[i][j] = (x == 0);		
//			}
//		}
 * 
 * 		Method 2 to create a random grid
 * 
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				x = rand.nextInt(101);
				if(x > (pWhite*100))
					grid.array[i][j] = false;
				else {
					nbWhite ++;
					grid.array[i][j] = true;
				}
			}
		}
		return grid;
	}
	*/
	/*
	 * create a random grid by filling an array with the good amount of 0s and 1s (from the percentage pWhite)
	 * this ensures that we have exactly the good percentage
	 * the array will be shuffled and we will send it back as a 2D array (representing a random grid)
	 */
	public static Grid createRandomGrid(int height, int width, float pWhite){
		Random rand = new Random();
		
		double nbCases = height*width;
		double nbWhiteCaseToSet = ((nbCases)*(double)pWhite);
		double nbWhiteCaseSet = 0;
		
		boolean[] randomArray = new boolean[height*width];
		boolean[][] arrayToReturn = new boolean[height][width];
		
		//Fill the simple array
		for(int i = 0;i<height;i++){
			for(int j = 0;j<height;j++){
				if(nbWhiteCaseSet>nbWhiteCaseToSet){
					randomArray[i*height+j] = false;
				}
				else{
					randomArray[i*height+j] = true;
					nbWhiteCaseSet++;
				}
			}
		}
		
		//Shuffle the randomArray
		
		for(int i = 0;i<100;i++){
			for(int j = 0;j<randomArray.length;j++){
				int randomPosition = rand.nextInt(randomArray.length);
				boolean temp = randomArray[j];
				randomArray[j] = randomArray[randomPosition];
				randomArray[randomPosition] = temp;
			}
		}
		
		//Fill the 2D array
		for(int i = 0; i < height; i++){
			for(int j = 0; j < width; j++){
				arrayToReturn[i][j]=randomArray[i*height+j];
			}
		}

		return new Grid(arrayToReturn);
	}

	public int getNbWhiteCase(){
		int nbW = 0;
		
		for(int i = 0;i<this.getArray().length;i++){
			for(int j = 0;j<this.getArray()[0].length;j++){
				if(this.getArray()[i][j])
					nbW++;
			}
		}
		return nbW;
	}
	
	public void displayGrid(){
		if(this.getArray() != null){
			for(int i = 0;i<this.getArray().length;i++){
				for(int j = 0;j<this.getArray()[0].length;j++){
					if(this.getArray()[i][j])
						System.out.print("0");
					else
						System.out.print("1");
				}
				System.out.println("");
			}
		}
	}

	public void displayHeights(){
		if(this.getArray() != null){
			if(this.getHeights() == null)
				this.createHeightsGrid();
			for(int i = 0;i<this.getArray().length;i++){
				for(int j = 0;j<this.getArray()[0].length;j++){
					System.out.print(""+this.getHeights()[i][j]);
				}
				System.out.println("");
			}
		}
	}

	//ACCESSORS

	public boolean[][] getArray() {
		return array;
	}

	public void setArray(boolean[][] array) {
		this.array = array;
	}

	public int[][] getHeights() {
		return heights;
	}

	public void setHeights(int[][] heights) {
		this.heights = heights;
	}
}
