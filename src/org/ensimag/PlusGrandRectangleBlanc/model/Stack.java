package org.ensimag.PlusGrandRectangleBlanc.model;

import java.util.Arrays;


public class Stack<E> {
	
	private StackElement firstElement;
	
	private class StackElement<T>{
		private StackElement previous;
		private T object;
		
		public StackElement(StackElement previous, T object) {
			super();
			this.previous = previous;
			this.object = object;
		}
	}
	
	public Stack(Class<E> c){
		this.firstElement=null;
	}
	
	public boolean isEmpty(){
		return firstElement==null;
	}
	
	public void push(E newElement){
		StackElement<E> newStackElement = new StackElement(firstElement, (Object)newElement);
		this.firstElement = newStackElement;
	}
	
	public E pop(){
		try{
			E toReturn = (E)firstElement.object;
			this.firstElement=this.firstElement.previous;
			return toReturn;
		}catch(NullPointerException e){
			return null;
		}
		
	}
	public E top(){
		try{
			E toReturn = (E)firstElement.object;
			return toReturn;
		}catch(NullPointerException e){
			return null;
		}
		
	}
}
