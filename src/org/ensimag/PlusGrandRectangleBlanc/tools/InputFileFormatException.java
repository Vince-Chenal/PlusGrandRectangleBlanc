package org.ensimag.PlusGrandRectangleBlanc.tools;

public class InputFileFormatException extends Exception {

	public InputFileFormatException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InputFileFormatException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InputFileFormatException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InputFileFormatException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InputFileFormatException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
}
