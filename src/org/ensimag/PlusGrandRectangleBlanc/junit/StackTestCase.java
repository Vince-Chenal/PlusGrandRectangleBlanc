package org.ensimag.PlusGrandRectangleBlanc.junit;

import static org.junit.Assert.*;

import org.ensimag.PlusGrandRectangleBlanc.model.Rectangle;
import org.ensimag.PlusGrandRectangleBlanc.model.Stack;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StackTestCase {

	private Rectangle r1,r2,r3,r4,r5;
	private Stack<Rectangle> stack;
	
	@Before
	public void setUp() throws Exception {
		
		//Instanciate the empty stack
		this.stack = new Stack<Rectangle>(Rectangle.class);
		
		//Instantiate a few rectangles
		r1 = new Rectangle(0, 0, 1, 1);
		r2 = new Rectangle(0, 0, 2, 2);
		r3 = new Rectangle(0, 0, 3, 3);
		r4 = new Rectangle(0, 0, 4, 4);
		r5 = new Rectangle(0, 0, 5, 5);
		
		this.stack.push(r1);
		this.stack.push(r2);
		this.stack.push(r3);
		this.stack.push(r4);
		this.stack.push(r5);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		//Pop tests
		assertEquals(r5, this.stack.pop());
		assertEquals(r4, this.stack.pop());
		assertEquals(r3, this.stack.pop());
		assertEquals(r2, this.stack.pop());
		assertEquals(r1, this.stack.pop());
		assertNull(this.stack.pop());
	}

}
