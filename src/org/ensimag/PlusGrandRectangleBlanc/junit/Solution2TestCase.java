package org.ensimag.PlusGrandRectangleBlanc.junit;

import static org.junit.Assert.*;

import org.ensimag.PlusGrandRectangleBlanc.algorithms.Solutions;
import org.ensimag.PlusGrandRectangleBlanc.model.Grid;
import org.ensimag.PlusGrandRectangleBlanc.model.Rectangle;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Solution2TestCase {

	private Grid[] grids;
	private Rectangle[] expectedRectangles;
	public static final int nbTests = 10;
	
	@Before
	public void setUp() throws Exception {
		
		grids = new Grid[Solution2TestCase.nbTests];
		
		//Populate the grid array
		for(int i = 0;i<Solution2TestCase.nbTests;i++){
			grids[i] = Grid.getGrid("Test/test"+(i+1)+".txt");
		}
		
		expectedRectangles = new Rectangle[Solution2TestCase.nbTests];
		
		expectedRectangles[0] = new Rectangle(0,1,1,1);
		expectedRectangles[1] = new Rectangle(1,0,1,1);
		expectedRectangles[2] = new Rectangle(0,0,2,2);
		expectedRectangles[3] = new Rectangle(0,0,0,0);
		expectedRectangles[4] = new Rectangle(0,0,1,2);
		expectedRectangles[5] = new Rectangle(0,2,1,2);
		expectedRectangles[6] = new Rectangle(0,3,3,2);
		expectedRectangles[7] = new Rectangle(2,2,3,6);
		expectedRectangles[8] = new Rectangle(1,2,2,4);
		expectedRectangles[9] = new Rectangle(2,2,2,2);
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("");
	}

	@Test
	public void test0() {
		assertEquals(grids[0].findBiggestWhiteRectangle(Solutions.Solution2),expectedRectangles[0]);
	}
	
	@Test
	public void test1() {
		assertEquals(grids[1].findBiggestWhiteRectangle(Solutions.Solution2),expectedRectangles[1]);
	}
	
	@Test
	public void test2() {
		assertEquals(grids[2].findBiggestWhiteRectangle(Solutions.Solution2),expectedRectangles[2]);
	}
	
	@Test
	public void test3() {
		assertEquals(grids[3].findBiggestWhiteRectangle(Solutions.Solution2),expectedRectangles[3]);
	}
	
	@Test
	public void test4() {
		assertEquals(grids[4].findBiggestWhiteRectangle(Solutions.Solution2),expectedRectangles[4]);
	}
	
	@Test
	public void test5() {
		assertEquals(grids[5].findBiggestWhiteRectangle(Solutions.Solution2),expectedRectangles[5]);
	}
	
	@Test
	public void test6() {
		assertEquals(grids[6].findBiggestWhiteRectangle(Solutions.Solution2),expectedRectangles[6]);
	}
	
	@Test
	public void test7() {
		assertEquals(grids[7].findBiggestWhiteRectangle(Solutions.Solution2),expectedRectangles[7]);
	}
	
	@Test
	public void test8() {
		assertEquals(grids[8].findBiggestWhiteRectangle(Solutions.Solution2),expectedRectangles[8]);
	}
	
	@Test
	public void test9() {
		assertEquals(grids[9].findBiggestWhiteRectangle(Solutions.Solution2),expectedRectangles[9]);
	}
	
}
