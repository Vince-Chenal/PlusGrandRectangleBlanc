package org.ensimag.PlusGrandRectangleBlanc.tests;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import org.ensimag.PlusGrandRectangleBlanc.algorithms.Solutions;
import org.ensimag.PlusGrandRectangleBlanc.model.Grid;
import org.ensimag.PlusGrandRectangleBlanc.model.Rectangle;
import org.ensimag.PlusGrandRectangleBlanc.tools.InputFileFormatException;

public class Test {

	public static void main(String[] args) throws InputFileFormatException{
//		testPerf();
		testFile();
//		testRandom();

	}

	
	/*
	 * This function is used to find the biggest Rectangle in the grid that is stocked 
	 * in the file dallage.txt (in same folder)
	 */
	public static void testFile(){
		try {
			//Retrieve Grid object from file
			Grid grid = Grid.getGrid("dallage.txt");
			//Display the rectangle grid
			grid.displayGrid();
			System.out.println("\n\n");
			//Find the biggest white rectangle using specified Algorithm
			Rectangle biggestRectangle = grid.findBiggestWhiteRectangle(Solutions.Solution1);
//			Rectangle biggestRectangle = grid.findBiggestWhiteRectangle(Solutions.Solution2);
//			Rectangle biggestRectangle = grid.findBiggestWhiteRectangle(Solutions.Solution3);
//			Rectangle biggestRectangle = grid.findBiggestWhiteRectangle(Solutions.Solution4);

			if(biggestRectangle.equals(Rectangle.nullRectangle))
				System.out.println("There is not rectangle in this Grid");
			else
				System.out.println("\n\nBiggest Rectangle : "+biggestRectangle);

		} catch (InputFileFormatException e) {
			e.printStackTrace();
		}
	}

	/*
	 * 
	 */
	public static void testRandom(){
		//Here we test each Solution on number of random grids.
		//We compare the results of each solution with the first solution. 
		//We know that the first solution is correct, this lets us verify that the other solutions return the right rectangle
		
		//				for(int i=0; i<100; i++){
		//					Grid grid = Grid.createRandomGrid(10, 10, 0.5f);
		//					Rectangle r1 = grid.findBiggestWhiteRectangle(Solutions.Solution1);
		//					Rectangle r2 = grid.findBiggestWhiteRectangle(Solutions.Solution2);
		//					Rectangle r3 = grid.findBiggestWhiteRectangle(Solutions.Solution3);
		//					Rectangle r4 = grid.findBiggestWhiteRectangle(Solutions.Solution4);
		//					
		//					if(r1.getNbCases() != r2.getNbCases()){ 
		//						grid.displayGrid();
		//						System.out.println("r2 wrong");
		//					}
		//					if(r1.getNbCases() != r3.getNbCases()){
		//						grid.displayGrid();
		//						System.out.println("r3 wrong");
		//					}
		//					if(r1.getNbCases() != r4.getNbCases()){
		//						grid.displayGrid();
		//						System.out.println("r4 wrong");
		//					}
		//				}
		//Here we calculate the average and the maximum cost of each algorithm
		int i;
		for(float d = 0; d<=1.1; d+=0.1){
			int max=0;
			int n = 0;
			for(i=1; i<100; i++){
				Counter.nbLoop56 = 0;
				Grid grid = Grid.createRandomGrid(5, 5, d);
				Rectangle r = grid.findBiggestWhiteRectangle(Solutions.Solution2);
				if(Counter.nbLoop56 > max){
					max = Counter.nbLoop56;
				}
				n+=Counter.nbLoop56;
			}
			System.out.println("dens "+d);
			System.out.println("moy "+n/i);
			System.out.println("max "+max+"\n");
		}
	}

	/*
	 * function that is used to calculate the performance values and write them in a file
	 * this was used to create the graphics in the report
	 */
	public static void testPerf() throws InputFileFormatException{

		long[] timeArray = new long[40];
		int compteur = 0;
		int sizemax = 1000;
		
		for(int size=200;size<sizemax;size+=20){
			//System.out.println("tour "+size);
			Grid grid = Grid.createRandomGrid(size, size, 0.8f);
			
			long before = 0;
			long after = 0;

//			before = System.currentTimeMillis();
			Counter.nbLoop56 = 0;
			grid.findBiggestWhiteRectangle(Solutions.Solution4);
			
//			after = System.currentTimeMillis();
			
			System.out.println(""+size);
			//System.out.println(after-before);
//			timeArray[compteur++] = after-before;
			timeArray[compteur++] = Counter.nbLoop56;
		}
		
		System.out.println("fini");
		
		PrintWriter writer;
		try {
			writer = new PrintWriter("excel.txt", "UTF-8");
			
			for(int i = 0;i<timeArray.length;i++){
				writer.println(""+timeArray[i]);
			}
			
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		//ENEGISTRER DANS UN FICHIER
	}

}
