package org.ensimag.PlusGrandRectangleBlanc.algorithms;

import org.ensimag.PlusGrandRectangleBlanc.model.Grid;
import org.ensimag.PlusGrandRectangleBlanc.model.Rectangle;
import org.ensimag.PlusGrandRectangleBlanc.tests.Counter;
/**
 * Represents the first solution. It is a simple but costly solution.
 * 
 * @author Vincent Chenal & Jeremia Oberle
 *
 */
public class Solution1 {

	/*
	 * prec: argument Grid grid
	 * grid != null because on creation the value of grid is tested and so compute cannot be called
	 * while grid is null
	 * 
	 * postc: return Rectangle biggestRectangle
	 * biggestRectanlge is Rectangle(0,0,0,0) if no Rectangle was found by the algorithm
	 */
	public static Rectangle compute(Grid grid){

		Rectangle rectanglePattern = null;
		Rectangle r = null;
		Rectangle biggestRectangle = new Rectangle(0, 0, 0, 0);

		//Find all possible rectangles
		for(int i = 1;i<=grid.getArray().length;i++){
			for(int j = 1;j<=grid.getArray()[0].length;j++){
				//Create new rectangle pattern
				rectanglePattern = new Rectangle(i, j);
				r = Solution1.isTheBiggestRectangle(grid, rectanglePattern);

				if(r != null){
					if(r.getNbCases()>biggestRectangle.getNbCases()){
						biggestRectangle = new Rectangle(r);
					}
				}
			}
		}

		return biggestRectangle;
	}

	/**
	 * Try to place the rectangle pattern (height,width) inside the grid and return a new 'positionned' rectangle(posY, posX, height,width)
	 * @param grid
	 * @param rectangle
	 * @return
	 */
	public static Rectangle isTheBiggestRectangle(Grid grid, Rectangle rectangle){

		Rectangle r = null;
		
		for(int i = 0;i<=grid.getArray().length-rectangle.getHeight();i++){
			rectangle.setPosY(i);
			for(int j = 0;j<=grid.getArray()[0].length-rectangle.getWidth();j++){
				rectangle.setPosX(j);
				if(grid.isRectangleValid(rectangle)){
					r=new Rectangle(rectangle);
				}
			}
		}
		return r;
	}
}
