package org.ensimag.PlusGrandRectangleBlanc.algorithms;

import org.ensimag.PlusGrandRectangleBlanc.model.Grid;
import org.ensimag.PlusGrandRectangleBlanc.model.Rectangle;
import org.ensimag.PlusGrandRectangleBlanc.tests.Counter;

public class Solution3 {

	/*
	 * prec: argument Grid grid
	 * grid != null because on creation the value of grid is tested and so compute cannot be called
	 * while grid is null
	 * 
	 * postc: return Rectangle biggestRectangle
	 * biggestRectanlge is Rectangle(0,0,0,0) if no Rectangle was found by the algorithm
	 */
	public static Rectangle compute(Grid grid){

		grid.createHeightsGrid();
		int[][] heights = grid.getHeights();
		
		Rectangle biggestRectangle = new Rectangle(0, 0, 0, 0);
		int min;
		for(int i = 0; i < grid.getArray().length; i++){ //i = line
			
			for(int start = 0; start < grid.getArray()[0].length; start++){ //start = posX of rectangle
				min = grid.getArray().length; //largest value we can find is the height of the array
				
				for(int end = start; end < grid.getArray()[0].length; end++){ //end = to check width of rectangle
					Counter.nbLoop56++;
					if(heights[i][end] != 0){
						
						if(heights[i][end] < min) //found a new minimum 
							min = heights[i][end];
						
						//create a new rectangle with the minimum height we found for this iteration
						Rectangle r = new Rectangle(i - min + 1, start, min, end - start + 1);

						if(r.getNbCases() > biggestRectangle.getNbCases()){
							biggestRectangle = new Rectangle(r);
						}
					}else{
						break;
					}
				}
			}
		}
		return biggestRectangle;
	}
}
