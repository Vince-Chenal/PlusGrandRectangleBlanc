package org.ensimag.PlusGrandRectangleBlanc.algorithms;

import org.ensimag.PlusGrandRectangleBlanc.model.Grid;
import org.ensimag.PlusGrandRectangleBlanc.model.Rectangle;
import org.ensimag.PlusGrandRectangleBlanc.tests.Counter;

public class Solution2{

	/*
	 * prec: argument Grid grid
	 * grid != null because on creation the value of grid is tested and so compute cannot be called
	 * while grid is null
	 * 
	 * postc: return Rectangle biggestRectangle
	 * biggestRectanlge is Rectangle(0,0,0,0) if no Rectangle was found by the algorithm
	 */
	public static Rectangle compute(Grid grid){
		
		Rectangle r = null;
		Rectangle biggestRectangle = new Rectangle(0, 0, 0, 0);
		int maxWidth;
		
		for(int i = 0; i < grid.getArray().length; i++){ //i = posY
			
			for(int j = 0; j < grid.getArray()[0].length; j++){ //j = posX

				if(grid.getArray()[i][j]){ //if not a black case
					r = new Rectangle(i, j, 0, 0);
					
					maxWidth = grid.getArray()[0].length - j; 
					for(int k = i; k < grid.getArray().length; k++){ //k = height
						//if the first case on the new line is black stop checking this rectangle
						if(!grid.getArray()[k][j]){ 
							break;  	
						}
						for(int l = j; l < grid.getArray()[0].length; l++){ //l = width
							Counter.nbLoop56++;
							//there is a black case in the grid so we continue on the next line
							//or we arrived on the width limit of the upper line
							if(!grid.getArray()[k][l] || l >= j + maxWidth){
								maxWidth = r.getWidth();
								break;  	
							}
							r.setWidth(r.getWidth() + 1);
						}
						r.setHeight(r.getHeight() + 1);
						
						if(r.getNbCases() > biggestRectangle.getNbCases()){
							biggestRectangle = new Rectangle(r);
						}
						r.setWidth(0);
					}
				}
			}
		}
		
		return biggestRectangle;
	}
}
