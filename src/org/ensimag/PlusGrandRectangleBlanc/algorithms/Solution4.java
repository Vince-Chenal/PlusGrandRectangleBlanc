package org.ensimag.PlusGrandRectangleBlanc.algorithms;

import org.ensimag.PlusGrandRectangleBlanc.model.Grid;
import org.ensimag.PlusGrandRectangleBlanc.model.Rectangle;
import org.ensimag.PlusGrandRectangleBlanc.model.Stack;
import org.ensimag.PlusGrandRectangleBlanc.tests.Counter;

public class Solution4 {

	/*
	 * prec: argument Grid grid grid != null because on creation the value of
	 * grid is tested and so compute cannot be called while grid is null
	 * 
	 * postc: return Rectangle biggestRectangle biggestRectanlge is
	 * Rectangle(0,0,0,0) if no Rectangle was found by the algorithm
	 */
	public static Rectangle compute(Grid grid) {

		int[][] heights = new int[grid.getArray().length][grid.getArray()[0].length];
		boolean[][] tab = grid.getArray();

		Rectangle biggestRectangle = new Rectangle(0, 0, 0, 0);
		int lastHeight = 0;
		Stack<Rectangle> stack = new Stack<Rectangle>(Rectangle.class);

		for (int i = 0; i < tab.length; i++) { // i = line
			lastHeight = 0;
			stack = new Stack<Rectangle>(Rectangle.class);
			for (int j = 0; j < tab[0].length; j++) { // j = column
				//If the current case is white
				if(tab[i][j]){

					if(i != 0){
						//We can increment the previous value
						heights[i][j] = heights[i - 1][j] + 1;
					}
					//If we are on the first line, the previous line doesn't exist.
					else
						heights[i][j] = 1;// So we set it to 1 (0+1)
				}
				//If the current case is black
				else{
					heights[i][j] = 0;//If the current case is black
				}

				Counter.nbLoop56++;
				if (heights[i][j] < lastHeight) {
					// close all bigger rectangles except the last one
					Rectangle r = stack.pop();
					// while the first rectangle on the stack is still
					// bigger and stack not empty
					// adjust and check size of rectangle r
					while (!stack.isEmpty() && (stack.top().getHeight() > heights[i][j])) {
						Counter.nbLoop56++;
						r.setWidth(j - r.getPosX());
						if (r.getNbCases() > biggestRectangle.getNbCases()) {
							biggestRectangle = new Rectangle(r);
						}
						r = stack.pop();
					}
					r.setWidth(j - r.getPosX());
					if (r.getNbCases() > biggestRectangle.getNbCases()) {
						biggestRectangle = new Rectangle(r);
					}
					// adjust height and posY of last rectangle found and
					// pop it back on stack
					if(heights[i][j] > 0){
						r.setHeight(heights[i][j]);
						r.setPosY(i - heights[i][j] + 1);
						stack.push(r);
					}
				}
				// add a new Rectangle to the stack which is strictly higher
				// than the last one added
				else if (heights[i][j] > lastHeight) {
					stack.push(new Rectangle(i - heights[i][j] + 1, j, heights[i][j], 0));
				}
				lastHeight = heights[i][j];
			}
			// close all rectangles left, adjust and check their size
			while (!stack.isEmpty()) {
				Counter.nbLoop56++;
				Rectangle r = new Rectangle(stack.pop());
				r.setWidth(grid.getArray()[0].length - r.getPosX());
				if (r.getNbCases() > biggestRectangle.getNbCases()) {
					biggestRectangle = new Rectangle(r);
				}
			}
		}
		return biggestRectangle;
	}
}
